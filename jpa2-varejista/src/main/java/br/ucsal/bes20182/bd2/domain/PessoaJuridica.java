package br.ucsal.bes20182.bd2.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	@Id
	@Column(name = "cnpj", columnDefinition = "char(11)")
	String cnpj;

	@Column(name = "nome", length = 40, nullable = false)
	String nome;

	@ManyToMany
	@JoinTable(name="tab_pessoa_juridica_ramo_atividade")
	List<RamoAtividade> ramosAtividade;

	@Column(name = "faturamento", columnDefinition = "numeric(10,2)", nullable = false)
	Double faturamento;

	@ManyToMany(mappedBy="clientes")
	List<Vendedor> vendedores;
}
