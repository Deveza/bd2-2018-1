package br.ucsal.bes20181.bd2.exemplojpa1.exemplo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.ucsal.bes20181.bd2.exemplojpa1.domain.Departamento;

public class ExemploJpa1 {

	public static void main(String[] args) {

		// https://www.objectdb.com/java/jpa

		EntityManagerFactory emf = null;

		try {

			emf = Persistence.createEntityManagerFactory("exemplo-jpa-pu");
			EntityManager em = emf.createEntityManager();

			Departamento departamento1 = new Departamento(1, "Informática");

			em.getTransaction().begin();

			em.persist(departamento1);

			em.getTransaction().commit();

		} finally {

			if (emf != null) {
				emf.close();
			}

		}

	}

}
