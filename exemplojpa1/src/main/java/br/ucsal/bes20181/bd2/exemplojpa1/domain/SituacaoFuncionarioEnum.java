package br.ucsal.bes20181.bd2.exemplojpa1.domain;

public enum SituacaoFuncionarioEnum {

	ATIVO("ATV"), FERIAS("FRS"), DEMITO("DES");

	private String codigo;

	private SituacaoFuncionarioEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoFuncionarioEnum valueOfCodigo(String codigo) {
		for (SituacaoFuncionarioEnum situacao : values()) {
			if (situacao.getCodigo().equalsIgnoreCase(codigo)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException();
	}

}
