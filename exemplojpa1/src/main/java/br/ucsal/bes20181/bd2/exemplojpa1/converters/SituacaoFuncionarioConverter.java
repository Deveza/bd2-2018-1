package br.ucsal.bes20181.bd2.exemplojpa1.converters;

import javax.persistence.AttributeConverter;

import br.ucsal.bes20181.bd2.exemplojpa1.domain.SituacaoFuncionarioEnum;

public class SituacaoFuncionarioConverter implements AttributeConverter<SituacaoFuncionarioEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoFuncionarioEnum attribute) {
		if (attribute != null) {
			return attribute.getCodigo();
		}
		return null;
	}

	@Override
	public SituacaoFuncionarioEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return SituacaoFuncionarioEnum.valueOfCodigo(dbData);
	}

}
